def alphabet_war(fight):
    #wpbs - left side 4321
    #mqdz - right side 4321
    L, R, BOMB = 0, 1, '*' 
    powers = {'w':(L,4), 'p':(L,3), 
             'b':(L,2), 's':(L,1),
             'm':(R,4), 'q':(R,3),
             'd':(R,2), 'z':(R,1)}
    
    battlefield = list(fight)
    def erease(idx):
        if (0 <= idx < len(battlefield) and
            battlefield[idx] != BOMB):
            battlefield[idx] = ''
                
    
    for idx,symbol in enumerate(battlefield):
        if symbol == BOMB:
            erease(idx-1)
            erease(idx+1)
    
    balance = {L:0, R:0}
    for symbol in battlefield:
        side, power = powers.get(symbol, (L,0))
        balance[side] += power 
    
    responses = {1:'Left side wins!',
                 0:"Let's fight again!",
                -1:'Right side wins!'}
    def signum(x):
        return (x > 0) - (x < 0)
    outcome = signum(balance[L] - balance[R])
    return responses[outcome]  